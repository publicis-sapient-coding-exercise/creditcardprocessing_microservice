package com.publicis.sapient.creditcardprocessing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CreditcardprocessingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreditcardprocessingApplication.class, args);
	}

}
