package com.publicis.sapient.creditcardprocessing.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.boot.jackson.JsonComponent;

import java.util.List;

@Data
@JsonComponent
public class AccountsResponse {

    @JsonProperty
    private String accountHolderName;

    @JsonProperty
    private List<CardsResponse> cards;

}
