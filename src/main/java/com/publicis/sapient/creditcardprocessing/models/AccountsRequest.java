package com.publicis.sapient.creditcardprocessing.models;

import com.publicis.sapient.creditcardprocessing.controller.validators.CreditCardNumberConstraint;
import lombok.Data;
import org.springframework.boot.jackson.JsonComponent;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@JsonComponent
public class AccountsRequest {

    @NotEmpty
    private String accountHolderName;

    @NotEmpty
    @Size(min = 1, max = 19, message = "Field 'cardNumber' must be between 1 and 19")
    @Pattern(regexp="^[\\d\\s]+$", message = "Invalid 'cardNumber' field")
    @CreditCardNumberConstraint
    private String cardNumber;

    @NotEmpty
    @Pattern(regexp="^\\d*\\.?\\d*$", message = "Invalid 'cardLimit' field")
    private String cardLimit;

}
