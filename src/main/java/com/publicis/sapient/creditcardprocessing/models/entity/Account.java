package com.publicis.sapient.creditcardprocessing.models.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "account")
@SequenceGenerator(name="seq", initialValue=3)
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "account_holder")
    private AccountHolder accountHolder;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(name = "account_cards",
            joinColumns = { @JoinColumn(name = "account_id") },
            inverseJoinColumns = { @JoinColumn(name = "card_id") })
    private List<Card> cards;

}
