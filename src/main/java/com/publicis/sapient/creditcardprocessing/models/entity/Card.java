package com.publicis.sapient.creditcardprocessing.models.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "card")
@SequenceGenerator(name="seq", initialValue=3)
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq")
    private Long id;

    @Column(name = "type")
    private String type;

    @Column(name = "card_number")
    private Long cardNumber;

    @Column(name = "currency")
    private String currency;

    @Column(name = "balance")
    private String balance;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "credit_limit")
    private BigDecimal creditLimit;

}
