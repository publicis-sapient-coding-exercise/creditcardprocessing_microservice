package com.publicis.sapient.creditcardprocessing.models.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "account_holder")
@SequenceGenerator(name="seq", initialValue=3)
public class AccountHolder {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq")
    private Long id;

    @Column(name = "name")
    private String name;

}
