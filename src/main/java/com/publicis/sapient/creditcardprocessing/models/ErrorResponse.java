package com.publicis.sapient.creditcardprocessing.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.jackson.JsonComponent;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonComponent
public class ErrorResponse {

    @JsonProperty
    private int httpCode;

    @JsonProperty
    private String httpMessage;

    @JsonProperty
    private List<ErrorData> errors;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonComponent
    public static class ErrorData {

        @JsonProperty
        private String field;

        @JsonProperty
        private String message;
    }

}
