package com.publicis.sapient.creditcardprocessing.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.boot.jackson.JsonComponent;

import java.math.BigDecimal;

@Data
@JsonComponent
public class CardsResponse {

    @JsonProperty
    private String cardNumber;

    @JsonProperty
    private String balance;

    @JsonProperty
    private BigDecimal amount;

    @JsonProperty
    private BigDecimal limit;

}
