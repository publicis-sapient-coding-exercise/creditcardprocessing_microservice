package com.publicis.sapient.creditcardprocessing.controller;

import com.publicis.sapient.creditcardprocessing.models.AccountsRequest;
import com.publicis.sapient.creditcardprocessing.models.AccountsResponse;
import com.publicis.sapient.creditcardprocessing.service.AccountsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Rest controller which exposes endpoints to get all accounts and add accounts.
 */

@RestController
@Validated
@RequestMapping(
        value = "/accounts",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class AccountsController {

    @Autowired
    private AccountsService accountsService;

    /**
     * Get endpoint which returns a list of accounts.
     * @return ResponseEntity<List<AccountsResponse>>
     */
    @GetMapping
    public ResponseEntity<List<AccountsResponse>> getAllAccounts(){
        final List<AccountsResponse> accounts = accountsService.getAllAccounts();
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }

    /**
     * Post endpoint which adds a new account.
     * The endpoint validates the request body before adding account.
     * Endpoint will return 204 - NO CONTENT if successful otherwise an error response will be returned.
     * @param accountsRequest - add to be added
     */
    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void addAccount(@RequestBody @Valid AccountsRequest accountsRequest){
        accountsService.addAccount(accountsRequest);
    }
}
