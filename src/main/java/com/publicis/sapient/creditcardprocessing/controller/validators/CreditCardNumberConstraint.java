package com.publicis.sapient.creditcardprocessing.controller.validators;

import javax.validation.Constraint;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;

@Documented
@Constraint(validatedBy = CreditCardNumberValidator.class)
@Target({FIELD, ANNOTATION_TYPE, TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
public @interface CreditCardNumberConstraint {

    String message() default "Invalid Credit Card Number";
    java.lang.Class<?>[] groups() default {};
    java.lang.Class<? extends javax.validation.Payload>[] payload() default {};
}
