package com.publicis.sapient.creditcardprocessing.controller.validators;

import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.stream.IntStream;

/**
 * Constraint Validator which performs the Luhn 10 validation on the credit card number.
 */

@Slf4j
public class CreditCardNumberValidator implements ConstraintValidator<CreditCardNumberConstraint, String> {

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return validateCreditCardNumber(s);
    }

    private boolean validateCreditCardNumber(String cardNumber) {

        int digits = cardNumber.length();

        int sum = 0;
        boolean isSecond = false;

        for (int i = digits - 1; i >= 0; i--)
        {
            int d = cardNumber.charAt(i) - '0';

            if (isSecond == true) {
                d = d * 2;
            }

            sum += d / 10;
            sum += d % 10;

            isSecond = !isSecond;
        }

        if (sum % 10 != 0) {
            return false;
        }

        return true;
    }
}
