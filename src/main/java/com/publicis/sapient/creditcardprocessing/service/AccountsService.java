package com.publicis.sapient.creditcardprocessing.service;

import com.publicis.sapient.creditcardprocessing.models.AccountsRequest;
import com.publicis.sapient.creditcardprocessing.models.AccountsResponse;
import com.publicis.sapient.creditcardprocessing.models.CardsResponse;
import com.publicis.sapient.creditcardprocessing.models.entity.Account;
import com.publicis.sapient.creditcardprocessing.models.entity.AccountHolder;
import com.publicis.sapient.creditcardprocessing.models.entity.Card;
import com.publicis.sapient.creditcardprocessing.repository.AccountsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Accounts service to get all accounts and add new accounts
 */

@Service
public class AccountsService {

    @Autowired
    private AccountsRepository accountsRepository;

    /**
     * Method returns a list of accounts
     * @return List<AccountsResponse>
     */
    public List<AccountsResponse> getAllAccounts(){

        final List<AccountsResponse> accountsResponses = new ArrayList<>();

        final List<Account> accounts = accountsRepository.findAll();

        if(!accounts.isEmpty()){
            accounts.forEach(account -> {

                final List<CardsResponse> cards = new ArrayList<>();
                account.getCards().forEach(card -> {
                    final CardsResponse cardsResponse = new CardsResponse();
                    cardsResponse.setCardNumber(String.valueOf(card.getCardNumber()));
                    cardsResponse.setBalance(card.getBalance());
                    cardsResponse.setAmount(card.getAmount());
                    cardsResponse.setLimit(card.getCreditLimit());
                    cards.add(cardsResponse);
                });

                final AccountsResponse accountsResponse = new AccountsResponse();
                accountsResponse.setAccountHolderName(account.getAccountHolder().getName());
                accountsResponse.setCards(cards);

                accountsResponses.add(accountsResponse);
            });

            return accountsResponses;
        }

        return null;
    }

    /**
     * Method adds a new account
     * @param accountsRequest
     */
    public void addAccount(final AccountsRequest accountsRequest){

        final AccountHolder accountHolder = new AccountHolder();
        accountHolder.setName(accountsRequest.getAccountHolderName());

        final Card card = new Card();
        card.setCurrency("GBP");
        card.setType("Credit Card");
        card.setBalance("CREDIT");
        card.setAmount(new BigDecimal(0.0));
        card.setCardNumber(Long.parseLong(accountsRequest.getCardNumber()));
        card.setCreditLimit(new BigDecimal(accountsRequest.getCardLimit()));

        final List<Card> cards = new ArrayList<>();
        cards.add(card);

        final Account account = new Account();
        account.setAccountHolder(accountHolder);
        account.setCards(cards);

        accountsRepository.save(account);
    }
}
