package com.publicis.sapient.creditcardprocessing.repository;

import com.publicis.sapient.creditcardprocessing.models.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountsRepository extends JpaRepository<Account, Long> {

}
