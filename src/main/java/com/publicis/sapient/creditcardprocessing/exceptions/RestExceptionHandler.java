package com.publicis.sapient.creditcardprocessing.exceptions;

import com.publicis.sapient.creditcardprocessing.models.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse methodArgumentNotValidExceptionHandler(final MethodArgumentNotValidException ex) {
        log.debug("Error in request {}", ex);

        return new ErrorResponse(
                HttpStatus.BAD_REQUEST.value(),
                HttpStatus.BAD_REQUEST.getReasonPhrase(),
                getErrorData(ex.getBindingResult().getFieldErrors()));
    }

    private List<ErrorResponse.ErrorData> getErrorData(final List<FieldError> fieldErrors){
        final List<ErrorResponse.ErrorData> errors = new ArrayList<>();
        fieldErrors.stream().forEach(field -> {
            errors.add(new ErrorResponse.ErrorData(field.getField(), field.getDefaultMessage()));
        });
        return errors;
    }

}
