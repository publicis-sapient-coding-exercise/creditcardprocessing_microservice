
-- DROP TABLE

DROP TABLE IF EXISTS account;
DROP TABLE IF EXISTS account_holder;
DROP TABLE IF EXISTS card;
DROP TABLE IF EXISTS account_cards;

-- CREATE TABLE

CREATE TABLE account_holder (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL
);

CREATE TABLE card (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  type VARCHAR(250) NOT NULL,
  card_number BIGINT(19) NOT NULL,
  currency VARCHAR(250) DEFAULT NULL,
  balance VARCHAR(250) DEFAULT NULL,
  amount DECIMAL(10,2) DEFAULT 0,
  credit_limit DECIMAL(10,2) DEFAULT NULL
);

CREATE TABLE account (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  account_holder INTEGER,
  FOREIGN KEY(account_holder) REFERENCES account_holder(id),
);

CREATE TABLE account_cards (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  account_id INTEGER,
  card_id INTEGER,
  FOREIGN KEY(account_id) REFERENCES account(id),
  FOREIGN KEY(card_id) REFERENCES card(id)
);

-- INSERT STATEMENTS

INSERT INTO card (id, type, card_number, currency, balance, amount, credit_limit) VALUES
    (1, 'Credit Card', '1111222233334444', 'GBP', 'DEBIT', '1045', '2000'),
    (2, 'Credit Card', '4444333322221111', 'GBP', 'CREDIT', '10.24', '5000');

INSERT INTO account_holder (id, name) VALUES
    (1, 'Alice'),
    (2, 'Bob');

INSERT INTO account (id, account_holder) VALUES
    (1, 1),
    (2, 2);

INSERT INTO account_cards (id, account_id, card_id) VALUES
    (1, 1, 1),
    (2, 2, 2);