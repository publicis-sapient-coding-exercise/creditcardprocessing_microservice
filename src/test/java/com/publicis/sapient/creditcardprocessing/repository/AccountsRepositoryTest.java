package com.publicis.sapient.creditcardprocessing.repository;

import com.publicis.sapient.creditcardprocessing.models.entity.Account;
import com.publicis.sapient.creditcardprocessing.models.entity.AccountHolder;
import com.publicis.sapient.creditcardprocessing.models.entity.Card;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountsRepositoryTest {

    @Autowired
    private AccountsRepository accountsRepository;

    @Test
    @Transactional
    public void getAllAccounts() {

        final List<Account> accounts = accountsRepository.findAll();

        assertNotNull(accounts);
        assertEquals(2, accounts.size());
    }

    @Test
    @Transactional
    public void addAccount() {

        final List<Account> accountBefore = accountsRepository.findAll();

        assertNotNull(accountBefore);
        assertEquals(2, accountBefore.size());

        final AccountHolder accountHolder = new AccountHolder();
        accountHolder.setName("TesterC");

        final Card card = new Card();
        card.setCurrency("GBP");
        card.setType("Credit Card");
        card.setAmount(new BigDecimal(0.0));
        card.setCardNumber(Long.parseLong("4111111111111111"));
        card.setCreditLimit(new BigDecimal(2000));

        final List<Card> cards = new ArrayList<>();
        cards.add(card);

        final Account account = new Account();
        account.setAccountHolder(accountHolder);
        account.setCards(cards);

        accountsRepository.save(account);
        final List<Account> accountsAfter = accountsRepository.findAll();

        assertNotNull(accountsAfter);
        assertEquals(3, accountsAfter.size());
    }

}
