package com.publicis.sapient.creditcardprocessing.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AccountsControllerTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void get_all_accounts() throws Exception {

        this.mockMvc
                .perform(get("/accounts").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[\n" +
                        "    {\n" +
                        "        \"accountHolderName\": \"TesterA\",\n" +
                        "        \"cards\": [\n" +
                        "            {\n" +
                        "                \"cardNumber\": \"4111111111111111\",\n" +
                        "                \"balance\": \"DEBIT\",\n" +
                        "                \"amount\": 10.00,\n" +
                        "                \"limit\": 1000\n" +
                        "            }\n" +
                        "        ]\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"accountHolderName\": \"TesterB\",\n" +
                        "        \"cards\": [\n" +
                        "            {\n" +
                        "                \"cardNumber\": \"5105105105105100\",\n" +
                        "                \"balance\": \"CREDIT\",\n" +
                        "                \"amount\": 20.00,\n" +
                        "                \"limit\": 2000\n" +
                        "            }\n" +
                        "        ]\n" +
                        "    }\n" +
                        "]"));
    }

    @Test
    public void add_account() throws Exception {

        this.mockMvc
                .perform(post("/accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"accountHolderName\":\"TesterC\",\"cardNumber\":\"5555555555554444\",\"cardLimit\":\"3000\"}"))
                .andExpect(status().isNoContent());

        this.mockMvc
                .perform(get("/accounts").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[\n" +
                        "    {\n" +
                        "        \"accountHolderName\": \"TesterA\",\n" +
                        "        \"cards\": [\n" +
                        "            {\n" +
                        "                \"cardNumber\": \"4111111111111111\",\n" +
                        "                \"balance\": \"DEBIT\",\n" +
                        "                \"amount\": 10.00,\n" +
                        "                \"limit\": 1000\n" +
                        "            }\n" +
                        "        ]\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"accountHolderName\": \"TesterB\",\n" +
                        "        \"cards\": [\n" +
                        "            {\n" +
                        "                \"cardNumber\": \"5105105105105100\",\n" +
                        "                \"balance\": \"CREDIT\",\n" +
                        "                \"amount\": 20.00,\n" +
                        "                \"limit\": 2000\n" +
                        "            }\n" +
                        "        ]\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"accountHolderName\": \"TesterC\",\n" +
                        "        \"cards\": [\n" +
                        "            {\n" +
                        "                \"cardNumber\": \"5555555555554444\",\n" +
                        "                \"balance\": \"CREDIT\",\n" +
                        "                \"amount\": 0,\n" +
                        "                \"limit\": 3000\n" +
                        "            }\n" +
                        "        ]\n" +
                        "    }\n" +
                        "]"));
    }

    @Test
    public void add_account_with_failed_card_number_luhn_check() throws Exception {
        this.mockMvc
                .perform(post("/accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"accountHolderName\":\"TesterC\",\"cardNumber\":\"4111111111111123\",\"cardLimit\":\"3000\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void add_account_with_invalid_card_number() throws Exception {
        this.mockMvc
                .perform(post("/accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"accountHolderName\":\"TesterC\",\"cardNumber\":\"41111111_INVALID\",\"cardLimit\":\"3000\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void add_account_with_characters_in_card_number() throws Exception {
        this.mockMvc
                .perform(post("/accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"accountHolderName\":\"TesterC\",\"cardNumber\":\"INVALIDCHARACTERS\",\"cardLimit\":\"3000\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void add_account_with_exceed_max_card_number() throws Exception {
        this.mockMvc
                .perform(post("/accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"accountHolderName\":\"TesterC\",\"cardNumber\":\"41111111111111111111\",\"cardLimit\":\"3000\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void add_account_with_invalid_card_limit() throws Exception {
        this.mockMvc
                .perform(post("/accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"accountHolderName\":\"TesterC\",\"cardNumber\":\"4111111111111111\",\"cardLimit\":\"3000_INVALID\"}"))
                .andExpect(status().isBadRequest());
    }
}
