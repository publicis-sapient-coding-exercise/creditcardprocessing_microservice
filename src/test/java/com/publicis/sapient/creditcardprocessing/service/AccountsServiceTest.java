package com.publicis.sapient.creditcardprocessing.service;

import com.publicis.sapient.creditcardprocessing.models.AccountsResponse;
import com.publicis.sapient.creditcardprocessing.models.entity.Account;
import com.publicis.sapient.creditcardprocessing.models.entity.AccountHolder;
import com.publicis.sapient.creditcardprocessing.repository.AccountsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountsServiceTest {

    @InjectMocks
    private AccountsService accountsService;

    @Mock
    private AccountsRepository accountsRepository;

    @Test
    @Transactional
    public void getAllAccounts(){

        final AccountHolder accountHolder = new AccountHolder();
        accountHolder.setName("Tester");

        final Account tester = new Account();
        tester.setAccountHolder(accountHolder);
        tester.setCards(new ArrayList<>());

        final List<Account> accounts = new ArrayList<>();
        accounts.add(tester);

        when(accountsRepository.findAll()).thenReturn(accounts);

        final List<AccountsResponse> accountsResponses = accountsService.getAllAccounts();

        assertNotNull(accountsResponses);
        assertEquals(1, accountsResponses.size());
    }

}
