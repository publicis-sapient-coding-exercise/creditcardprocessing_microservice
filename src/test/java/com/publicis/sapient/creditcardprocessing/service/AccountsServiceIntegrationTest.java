package com.publicis.sapient.creditcardprocessing.service;

import com.publicis.sapient.creditcardprocessing.models.AccountsRequest;
import com.publicis.sapient.creditcardprocessing.models.AccountsResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountsServiceIntegrationTest {

    @Autowired
    private AccountsService accountsService;

    @Test
    @Transactional
    public void getAllAccounts(){
        final List<AccountsResponse> accounts = accountsService.getAllAccounts();

        assertNotNull(accounts);
        assertEquals(2, accounts.size());
    }

    @Test
    @Transactional
    public void addAccount(){
        final List<AccountsResponse> accountsBefore = accountsService.getAllAccounts();

        assertNotNull(accountsBefore);
        assertEquals(2, accountsBefore.size());

        final AccountsRequest accountsRequest = new AccountsRequest();
        accountsRequest.setAccountHolderName("TesterC");
        accountsRequest.setCardNumber("4111111111111111");
        accountsRequest.setCardLimit("3000");

        accountsService.addAccount(accountsRequest);
        final List<AccountsResponse> accountsAfter = accountsService.getAllAccounts();

        assertNotNull(accountsAfter);
        assertEquals(3, accountsAfter.size());
    }

}
