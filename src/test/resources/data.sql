
-- DROP TABLE

DROP TABLE IF EXISTS account;
DROP TABLE IF EXISTS account_holder;
DROP TABLE IF EXISTS card;
DROP TABLE IF EXISTS account_cards;

-- CREATE TABLE

CREATE TABLE account_holder (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL
);

CREATE TABLE card (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  type VARCHAR(250) NOT NULL,
  card_number VARCHAR(250) NOT NULL,
  currency VARCHAR(250) DEFAULT NULL,
  balance VARCHAR(250) DEFAULT NULL,
  amount DECIMAL(10,2) DEFAULT 0,
  credit_limit DECIMAL(10,2) DEFAULT NULL
);

CREATE TABLE account (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  account_holder INTEGER,
  FOREIGN KEY(account_holder) REFERENCES account_holder(id),
);

CREATE TABLE account_cards (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  account_id INTEGER,
  card_id INTEGER,
  FOREIGN KEY(account_id) REFERENCES account(id),
  FOREIGN KEY(card_id) REFERENCES card(id)
);

-- INSERT STATEMENTS

INSERT INTO card (id, type, card_number, currency, balance, amount, credit_limit) VALUES
    (8, 'Credit Card', '4111111111111111', 'GBP', 'DEBIT', '10.00', '1000'),
    (9, 'Credit Card', '5105105105105100', 'GBP', 'CREDIT', '20.00', '2000');

INSERT INTO account_holder (id, name) VALUES
    (8, 'TesterA'),
    (9, 'TesterB');

INSERT INTO account (id, account_holder) VALUES
    (8, 8),
    (9, 9);

INSERT INTO account_cards (id, account_id, card_id) VALUES
    (8, 8, 8),
    (9, 9, 9);