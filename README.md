# Credit Card Processing Microservice

The Credit Card Processing microservice which exposes endpoints to get a list of accounts and to add a new account.

# Prerequisites
*  Java 8
*  Apache Maven

# Setup
1.  Checkout project
2.  Open terminal and navigate to project folder
3.  Run Maven command `mvn clean install`

# Start Application

Run `mvn spring-boot:run`

# Endpoints

*  GET /accounts
*  POST /accounts

Please see postman collections in the 'postman' folder on the root directory

# GET /accounts

This endpoint will return a JSON with a list of accounts.

**Example Request**

* Request Header: `Content-Type: application/json`

**Example Response:**

```
[
    {
        "accountHolderName": "Alice",
        "cards": [
            {
                "cardNumber": "1111 2222 3333 4444",
                "balance": "DEBIT",
                "amount": 1045,
                "limit": 2000
            }
        ]
    },
    {
        "accountHolderName": "Bob",
        "cards": [
            {
                "cardNumber": "4444 3333 2222 1111",
                "balance": "CREDIT",
                "amount": 10.24,
                "limit": 5000
            }
        ]
    }
]
```

# POST /accounts

This endpoint will a new account

**Example Request:**

* Request Header: `Content-Type: application/json`
* Request Body: `{"accountHolderName":"TesterC","cardNumber":"411111111111111","cardLimit":"3000"}`

**Example Response:**

`204 - NO CONTENT`